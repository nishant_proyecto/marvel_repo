# Marvel Assesment Source Code

### Android Studio and Gradle Configuration

#### Prerequisites
 - Java 7 (or newer)
 - Android Studio
 - Gradle 2.2 (or newer)

#### How to configure source code in Android Studio
 1. Clone the repository.
 2. Import repository folder in Android Studio as Existing Studio Project.
 3. Add Lombok Plugin in Android Studio. To integrate Lombok follow below link
    https://projectlombok.org/features/index.html

#### Mipmap folders in res
The mipmap folders are for placing your app icons in only. Any other drawable assets you use should be placed in the relevant drawable folders as before.
According to google's post at http://android-developers.blogspot.co.uk/2014/10/getting-your-apps-ready-for-nexus-6-and.html, 
It’s best practice to place your app icons in mipmap- folders (not the drawable- folders) because they are used at resolutions different from the device’s current density.