package com.marvel;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

import com.marvel.di.component.ApplicationComponent;
import com.marvel.di.component.DaggerApplicationComponent;
import com.marvel.di.module.ApplicationModule;
import com.marvel.ui.utils.HashUtil;
import com.pixplicity.easyprefs.library.Prefs;

import timber.log.Timber;

/**
 * Created by nishantdande on 05/04/17.
 */

public class MarvelApplication extends Application {

    ApplicationComponent applicationComponent;

    static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;

        // Init Logger
        Timber.plant(new Timber.DebugTree());
        Timber.tag("marvel");

        // Init Shared Preferences
        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        // Init hash value
        HashUtil.init();

        initApplicationComponent();
    }

    public static Context getContext() {
        return context;
    }

    private void initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder().
                applicationModule(new ApplicationModule()).build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
