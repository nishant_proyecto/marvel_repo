package com.marvel.mvp.presenter;

import com.marvel.BuildConfig;
import com.marvel.MarvelApplication;
import com.marvel.R;
import com.marvel.api.ComicsApiService;
import com.marvel.mvp.model.Result;
import com.marvel.mvp.view.MainView;
import com.marvel.ui.perf.EndpointPerf;

import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by nishantdande on 05/04/17.
 */

public class ComicsPresenter extends BasePresenter<MainView> {

    @Inject
    protected ComicsApiService comicsApiService;

    private ArrayList<Result> results;


    @Inject
    protected ComicsPresenter() {
    }

    public void getComics(int limits) {
        getView().onShowDialog(MarvelApplication.getContext().getString(R.string.loading_comics));
        comicsApiService.getComics(EndpointPerf.getTimeStamp(), limits, BuildConfig.PublicKey, EndpointPerf.getHashKey())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
        .subscribe(comicResponse -> {

            getView().onHideDialog();
            results = comicResponse.getData().getResults();

            getView().onComicsLoaded(results);

        }, throwable -> {
            getView().onError(throwable);
            getView().onHideDialog();

        });
    }

    public void filterComic(float price){

        ArrayList<Result> results = new ArrayList<>();

        for (Result result : this.results){
            if (Float.parseFloat(result.getPrices().get(0).getPrice()) >= price){
                results.add(result);
            }
        }

        getView().onComicsLoaded(results);

        // RxJava using Stream but Compatible with sdk 24+
//        results.stream()
//                .filter(result -> Integer.parseInt(result.getPrices().get(0).getPrice()) >= price)
//                .collect(Collectors.toList());
    }
}
