package com.marvel.mvp.model;

import com.google.gson.annotations.SerializedName;


/**
 * Created by nishantdande on 05/04/17.
 */

@lombok.Data
public class ComicResponse {

    @SerializedName("code")
    private String code;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    Data data;



}
