package com.marvel.mvp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nishantdande on 05/04/17.
 */

@lombok.Data
public class Data {

    @SerializedName("results")
    private ArrayList<Result> results;

}
