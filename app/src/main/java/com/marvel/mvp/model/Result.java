package com.marvel.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.*;

/**
 * Created by nishantdande on 05/04/17.
 */

@lombok.Data
public class Result implements Parcelable {

    private String title;

    private String description;

    @SerializedName("thumbnail")
    private Thumbnail thumbnail;

    private String pageCount;

    @SerializedName("prices")
    private ArrayList<Price> prices;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeParcelable(this.thumbnail, flags);
        dest.writeString(this.pageCount);
        dest.writeList(this.prices);
    }

    protected Result(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.thumbnail = in.readParcelable(Thumbnail.class.getClassLoader());
        this.pageCount = in.readString();
        this.prices = new ArrayList<Price>();
        in.readList(this.prices, Price.class.getClassLoader());
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
