package com.marvel.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import lombok.*;

/**
 * Created by nishantdande on 05/04/17.
 */

@lombok.Data
public class Price implements Parcelable {

    @SerializedName("type")
    private String type;

    @SerializedName("price")
    private String price;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeString(this.price);
    }

    protected Price(Parcel in) {
        this.type = in.readString();
        this.price = in.readString();
    }

    public static final Parcelable.Creator<Price> CREATOR = new Parcelable.Creator<Price>() {
        @Override
        public Price createFromParcel(Parcel source) {
            return new Price(source);
        }

        @Override
        public Price[] newArray(int size) {
            return new Price[size];
        }
    };
}
