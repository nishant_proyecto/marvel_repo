package com.marvel.mvp.view;

import com.marvel.mvp.model.Result;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by nishantdande
 */

public interface MainView extends BaseView {
    void onComicsLoaded(ArrayList<Result> comics);

    void onShowDialog(String s);

    void onShowToast(String s);

    void onHideDialog();

}
