package com.marvel.mvp.view;

/**
 * Created by nishantdande
 */

public interface BaseView {

    void onError(Throwable throwable);
}
