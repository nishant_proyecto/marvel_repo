package com.marvel.di.module;

import com.marvel.api.ComicsApiService;
import com.marvel.di.scope.PerActivity;
import com.marvel.mvp.view.MainView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by nishantdande
 */

@PerActivity
@Module
public class ComicsModule {

    MainView mainView;

    public ComicsModule(MainView mainView) {
        this.mainView = mainView;
    }

    @PerActivity
    @Provides
    ComicsApiService provideComicsApiService(Retrofit retrofit){
        return retrofit.create(ComicsApiService.class);
    }


    @PerActivity
    @Provides
    public MainView provideView() {
        return mainView;
    }
}
