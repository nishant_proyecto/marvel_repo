package com.marvel.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by nishantdande
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
