package com.marvel.di.component;

import com.marvel.di.module.ComicsModule;
import com.marvel.di.scope.PerActivity;
import com.marvel.ui.activity.MainActivity;

import dagger.Component;

/**
 * Created by nishantdande on 08/12/16.
 */

@PerActivity
@Component(modules = ComicsModule.class, dependencies = ApplicationComponent.class)
public interface ComicsComponent {

    void inject(MainActivity mainActivity);
}
