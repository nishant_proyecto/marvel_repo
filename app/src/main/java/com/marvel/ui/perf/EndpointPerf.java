package com.marvel.ui.perf;

import com.pixplicity.easyprefs.library.Prefs;

/**
 * Created by nishantdande on 05/04/17.
 */

public class EndpointPerf {

    public static void setTimeStamp(String timestamp){
        Prefs.putString("timestamp", timestamp);
    }

    public static String getTimeStamp(){
        return Prefs.getString("timestamp", "");
    }

    public static void setHashKey(String hashKey){
        Prefs.putString("hashKey", hashKey);
    }

    public static String getHashKey(){
        return Prefs.getString("hashKey", "");
    }

}
