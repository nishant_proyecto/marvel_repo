package com.marvel.ui.error;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.marvel.ui.utils.AppUtils;

import retrofit2.Response;
import timber.log.Timber;


public class APIErrorHandler extends Handler {

    private Activity activity;

    public APIErrorHandler(Activity activity) {
        super();
        this.activity = activity;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        Bundle bundle = msg.getData();
        AppUtils.displayShortSnackbar(activity, bundle.getString("msg"));
    }

    public void handleError(Throwable throwable) {
        Timber.e(throwable);
        String msg;
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            Response response = httpException.response();
            msg = response.errorBody().toString();
        }else{
            msg = throwable.getMessage();
        }

        Message message = obtainMessage();
        Bundle bundle= new Bundle();
        bundle.putString("msg", msg);
        message.setData(bundle);
        sendMessage(message);
    }
}