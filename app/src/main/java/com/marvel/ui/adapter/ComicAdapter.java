package com.marvel.ui.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.marvel.MarvelApplication;
import com.marvel.R;
import com.marvel.mvp.model.Result;
import com.marvel.ui.utils.Router;

import java.util.ArrayList;

/**
 * Created by nishantdande on 06/04/17.
 */

public class ComicAdapter extends RecyclerView.Adapter<ComicAdapter.ViewHolder> {

    private Activity activity;
    ArrayList<Result> mDataset;

    public ComicAdapter(Activity activity,ArrayList<Result> mDataset) {
        if (this.mDataset!=null && this.mDataset.size()>0){
            this.mDataset.clear();
        }
        this.mDataset = mDataset;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comics_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (mDataset!=null && mDataset.size()>0){
            Result result= mDataset.get(position);
            holder.title.setText(result.getTitle());

            String img = String.format("%s/landscape_amazing.%s", result.getThumbnail().getPath(),
                    result.getThumbnail().getExtension());
            Glide.with(MarvelApplication.getContext())
                    .load(img)
                    .into(holder.thumbnail);

            holder.itemView.setOnClickListener(view -> {
                if (result!=null){
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("comicBundle", result);
                    Router.launchComicDetailActivity(activity, bundle);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public ImageView thumbnail;
        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.comictitle);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            view.setTag(this);
        }
    }
}
