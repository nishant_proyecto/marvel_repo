package com.marvel.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.marvel.MarvelApplication;
import com.marvel.di.component.ApplicationComponent;
import com.marvel.ui.error.APIErrorHandler;

import butterknife.ButterKnife;


/**
 * Created by nishantdande
 */

public abstract class BaseActivity extends AppCompatActivity {

    public abstract int getContentView();

    APIErrorHandler apiErrorHandler = new APIErrorHandler(this);

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        onViewReady(savedInstanceState, getIntent());
    }

    protected void resolveDaggerDependency() {}

    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        resolveDaggerDependency();
        //To be used by child activities
    }

    protected void showDialog(@NonNull String message){
        if (mProgressDialog == null){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    protected void closeDialog(){
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
            mProgressDialog.dismiss();
        }
    }

    protected ApplicationComponent getApplicationComponent(){
        return ((MarvelApplication)getApplication()).getApplicationComponent();
    }




}
