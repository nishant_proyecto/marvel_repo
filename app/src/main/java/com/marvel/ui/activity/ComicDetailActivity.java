package com.marvel.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.marvel.MarvelApplication;
import com.marvel.R;
import com.marvel.mvp.model.Result;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nishantdande on 06/04/17.
 */

public class ComicDetailActivity extends BaseActivity{


    @BindView(R.id.comicImg)
    ImageView mComicImage;
    @BindView(R.id.comicTitle)
    TextView mTitle;
    @BindView(R.id.author)
    TextView mAuthor;
    @BindView(R.id.count)
    TextView mPageCount;
    @BindView(R.id.price)
    TextView mPrice;
    @BindView(R.id.description)
    TextView mDescription;

    @Override
    public int getContentView() {
        return R.layout.comic_detail_activity;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = intent.getExtras();
        if (extras!=null){
            Result result = (Result) extras.get("comicBundle");

            updateUi(result);
        }
    }

    private void updateUi(Result result) {
        String img = String.format("%s/landscape_amazing.%s", result.getThumbnail().getPath(),
                result.getThumbnail().getExtension());
        Glide.with(MarvelApplication.getContext())
                .load(img)
                .into(mComicImage);
        mTitle.setText(result.getTitle());
        mDescription.setText(result.getDescription());
        mDescription.setMovementMethod(new ScrollingMovementMethod());
        mPageCount.setText(getString(R.string.pagecount_s, result.getPageCount()));
        mPrice.setText(getString(R.string.price, result.getPrices().get(0).getPrice()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
