package com.marvel.ui.activity;

import android.content.Intent;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.marvel.R;
import com.marvel.di.component.DaggerComicsComponent;
import com.marvel.di.module.ComicsModule;
import com.marvel.mvp.model.Result;
import com.marvel.mvp.presenter.ComicsPresenter;
import com.marvel.mvp.view.MainView;
import com.marvel.ui.adapter.ComicAdapter;
import com.marvel.ui.perf.EndpointPerf;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends BaseActivity implements MainView{

    @Inject
    ComicsPresenter mPresenter;

    @BindView(R.id.comicsView)
    RecyclerView mRecyclerView;

    @BindView(R.id.searchComic)
    EditText mSearchComic;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;



    @Override
    public int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        ButterKnife.bind(this);

        mPresenter.getComics(100);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSearchComic.addTextChangedListener(textWatcher);

        Timber.i(Prefs.getAll()+"");
    }

    @Override
    protected void resolveDaggerDependency() {
        super.resolveDaggerDependency();
        DaggerComicsComponent.builder().applicationComponent(getApplicationComponent())
                .comicsModule(new ComicsModule(this)).build().inject(this);
    }


    @Override
    public void onComicsLoaded(ArrayList<Result> comics) {
        // specify an adapter (see also next example)
        mAdapter = new ComicAdapter(this, comics);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onShowDialog(String s) {
        showDialog(s);
    }

    @Override
    public void onShowToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onHideDialog() {
        closeDialog();
    }

    @Override
    public void onError(Throwable throwable) {
        apiErrorHandler.handleError(throwable);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (!TextUtils.isEmpty(charSequence))
                mPresenter.filterComic(Float.parseFloat(String.valueOf(charSequence)));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
}
