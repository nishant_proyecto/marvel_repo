package com.marvel.ui.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.marvel.ui.activity.ComicDetailActivity;

/**
 * Created by nishantdande on 06/04/17.
 */

public class Router {

    /**
     * Launch Comic Detail Screen Activity
     * @param activity
     */
    public static void launchComicDetailActivity(Activity activity, Bundle bundle) {
        Intent intent = new Intent(activity, ComicDetailActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }


}
