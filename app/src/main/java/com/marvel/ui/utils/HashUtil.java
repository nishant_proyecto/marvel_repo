package com.marvel.ui.utils;

import com.marvel.BuildConfig;
import com.marvel.ui.perf.EndpointPerf;

import timber.log.Timber;

/**
 * Created by nishantdande on 05/04/17.
 */

public class HashUtil {

    /**
     * Initialize to get Hash key
     */
    public static void  init() {
        try {
            String ts = String.valueOf(System.currentTimeMillis());

            // Set Timestamp in Prefs
            EndpointPerf.setTimeStamp(ts);


            String hashString = EndpointPerf.getTimeStamp()+BuildConfig.PrivateKey+BuildConfig.PublicKey;

            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(hashString.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            String hashKey  = sb.toString();

            // Set hashkey in Prefs
            EndpointPerf.setHashKey(hashKey);

        } catch (java.security.NoSuchAlgorithmException e) {
            Timber.e(e);
        }
    }
}
