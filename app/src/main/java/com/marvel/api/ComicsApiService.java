package com.marvel.api;

import com.marvel.mvp.model.ComicResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nishantdande
 */

public interface ComicsApiService {

    @GET("v1/public/comics?")
    Observable<ComicResponse> getComics(@Query("ts") String timestamp,
                                        @Query("limit") int limit,
                                        @Query("apikey") String publicKey,
                                        @Query("hash") String hashKey);



}
